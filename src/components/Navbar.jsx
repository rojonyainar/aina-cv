import Link from "next/link";
import React from "react";
import { useRouter } from "next/router";
import { DribbbleIcon, GithubIcon, LinkedInIcon, MoonIcon, SunIcon } from "./Icons";
import { motion } from "framer-motion";
import useThemeSwitcher from "./hooks/useThemeSwitcher";

const CustomLink = ({ href, title, className = "" }) => {
  const router = useRouter();
  return (
    <Link href={href} className={`${className} relative group`}>
      {title}
      <span
        className={`h-[1px] inline-block bg-dark absolute left-0 -bottom-1
      group-hover:w-full transition-[width] ease duration-300 
      ${router.asPath === href ? "w-full" : "w-0"}
      dark:bg-light 
      `}
      >
        &nbsp;
      </span>
    </Link>
  );
};

const Navbar = () => {

  const [mode, setMode] = useThemeSwitcher();
  return (
    <header className="w-full px-32 py-8 font-medium text-xl flex items-center justify-between dark:text-light">
      <nav>
        <CustomLink href="/" title="Accueil" className="mr-4" />
        <CustomLink href="/about" title="A propos" className="mx-4" />
        <CustomLink href="/parcours" title="Parcours" className="ml-4" />
        <CustomLink href="/projects" title="Projets" className="mx-4" />
      </nav>

      <nav className="flex items-center justify-center flex-wrap">
        <motion.a
          href="https://www.linkedin.com/in/rojonyainarakoto/"
          target={"_blank"}
          whileHover={{ y: -2 }}
          whileTap={{ scale: 0.9 }}
          className="w-8 mr-3"
        >
          <LinkedInIcon />
        </motion.a>
        <motion.a
          href="https://github.com/rojoniainarakoto"
          target={"_blank"}
          whileHover={{ y: -2 }}
          whileTap={{ scale: 0.9 }}
          className="w-8 mr-3"
        >
          <GithubIcon />
        </motion.a>
        <motion.a
          href="https://www.facebook.com/dingo.ryoji/"
          target={"_blank"}
          whileHover={{ y: -2 }}
          whileTap={{ scale: 0.9 }}
          className="w-8 mr-3"
        >
          <DribbbleIcon />
        </motion.a>

        <button 
         onClick={() => setMode(mode === "light" ? "dark" : "light")}
         className={`w-8 ml-3 flex items-center justify-center rounded-full p-1 
         ${mode === "light" ? "bg-dark text-light" : "bg-light text-dark"}
         `}
        >
          {
            mode === "dark" ? <SunIcon className={"fill-dark"}/> : <MoonIcon className={"fill-dark"}/>
          }
        </button>
      </nav>
    </header>
  );
};

export default Navbar;
