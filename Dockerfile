# Étape 1 : Construire l'application Next.js
FROM node:20-alpine3.17 AS build
WORKDIR /app

# Copier les fichiers de package.json et yarn.lock
COPY package*.json yarn.lock ./
RUN yarn install

# Copier le reste des fichiers de l'application
COPY . .

# Construire l'application Next.js
RUN yarn build

# Étape 2 : Exécuter l'application construite
FROM node:20-alpine3.17 AS production
ENV NODE_ENV=production
WORKDIR /app

# Copier les fichiers de package.json et yarn.lock
COPY package*.json yarn.lock ./
RUN yarn install --production

# Copier les fichiers construits à partir de l'étape précédente
COPY --from=build /app/.next ./.next

# Exposer le port sur lequel l'application va écouter
EXPOSE 3000

# Commande pour démarrer l'application
CMD ["yarn", "start"]
